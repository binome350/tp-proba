%% Paramétrage de la classe
nbRealisations = 1000;
nbvaleurGenerees = 1000;
probaLoiBinomiale = 0.5;

%% Génération Von Neuman
values0 = LoiNormale( nbvaleurGenerees, probaLoiBinomiale ,nbRealisations);

%% Histogramme
%% Affichage des transformées inverses
figure
title('Histogramme')
nbLigne2 = 2;
nbColon2 = 2;
nbColHistogramme = 100;
histogram(values0,nbColHistogramme); title('Loi Normale')
