function [ values ] = LoiBinomiale( n, p, nbvaleurGenerees)
% Tableau de sortie
values = 1:nbvaleurGenerees;

    for iterations = 1:nbvaleurGenerees
        U = rand(1,n);
        %On cr�� un tableau de bool�an avec un 1 si il y a eu un succ�s
        tableauBoolean = (U<p);
        % On compte les succ�s
        nbSucces = sum(tableauBoolean);
        %On stocke
        values(iterations) = nbSucces;
    end
    
end