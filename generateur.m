function [ values ] = generateur( numAlgo, nbvaleurGenerees )

%% Variables d�clar�es pr�c�demment
global sVN;
global sRANDU;
global sMT;
global sSM;
% Tableau de sortie
values = 1:nbvaleurGenerees;
% Param�tre pour algorithme RANDU
aRANDU = 65539;
bRANDU = 0;
mRANDU = power(2,31);
% Param�tre pour algorithme Standard Minimal
aSM = 16807;
bSM = 0;
mSM = power(2,31)-1;
% Param�tre pour Mersenne-Twister
mMERSENNE = power(2,31)-1;

%% Choix de l'algorithme de g�n�ration de nombre al�atoire
switch numAlgo
    %% fonction VonNeumann � valeurs dans 0,..., 9999
    case 0
        %display('Mode : VonNeuman ');
        %display(['Graine courante : ',num2str(sVN)]);
        
        % Initialisation = r�cup�ratin de la graine courante
        nbAleatoireCourant = sVN;
        
        % Pour toutes les valeurs devant �tre g�n�r�es
        for iterations = 1:nbvaleurGenerees
            %On met la valeur courante au carr�
            nbAleatoireCourant = power(nbAleatoireCourant,2);
            
            %On la limite � 9999 par suppression de chiffres
            while nbAleatoireCourant > 9999
                strTMPCar = num2str(nbAleatoireCourant);
                %display(['d�part : ', strTMPCar]);
                strTMPCar = strTMPCar(2:end-1);
                %display(['arriv�e : ',strTMPCar]);
                nbAleatoireCourant = str2num(strTMPCar) +1 ;
            end
            %On ajoute la valeur au tableau de r�sultat
            values(iterations) = nbAleatoireCourant;
            
        end
        %On pr�pare la prochaine graine
        sVN = values(nbvaleurGenerees);
        %display(['Prochaine graine : ',num2str(sVN)]);
        
        %% fonction RANDU
    case 1
        %display('Mode : RANDU ');
        %display(['Graine courante : ',num2str(sRANDU)]);
        
        % Initialisation = r�cup�ratin de la graine courante
        nbAleatoireCourant = sRANDU;
        
        % Pour toutes les valeurs devant �tre g�n�r�es
        for iterations = 1:nbvaleurGenerees
            %G�n�ration du prochain nombre par congruences lin�aires
            nbAleatoireCourant = mod(aRANDU*nbAleatoireCourant+bRANDU,mRANDU);
            values(iterations) = nbAleatoireCourant;
        end
        %On pr�pare la prochaine graine
        sRANDU = values(nbvaleurGenerees);
        %display(['Prochaine graine : ',num2str(sRANDU)]);
        
        %% fonction Mersenne Twister (avec utilisation de randi)
    case 2
        %display('Mode : MERSENNE ');
        %display(['Graine courante : ',num2str(sMT)]);
        
        %G�n�ration de la graine
        rng(sMT,'twister');
        %G�n�ration d'un ensemble de valeurs al�atoires
        values = randi(mMERSENNE,1,nbvaleurGenerees);
        %Stockage pour la prochaine graine
        sMT = values(nbvaleurGenerees);
        
        %display(['Prochaine graine : ',num2str(sMT)]);
        
        %% fonction Standard Minimal (avec utilisation de randi possible)
    case 3
        %display('Mode : Standard Minimal ');
        %display(['Graine courante : ',num2str(sSM)]);
        
        % Initialisation = r�cup�ratin de la graine courante
        nbAleatoireCourant = sSM;
        
        % Pour toutes les valeurs devant �tre g�n�r�es
        for iterations = 1:nbvaleurGenerees
            
            % Pour toutes les valeurs devant �tre g�n�r�es
            nbAleatoireCourant = mod(aSM*nbAleatoireCourant+bSM,mSM);
            values(iterations) = nbAleatoireCourant;
            
        end
        
        %On pr�pare la prochaine graine
        sSM = values(nbvaleurGenerees);
        
        %display(['Prochaine graine : ',num2str(sSM)]);
        
        %% nombre non trait� : cas d'exception
    otherwise
        values = 0;
end