function [ values, temps ] = simuRejet( nbvaleurGenerees)
% Tableau de sortie
values = 1:nbvaleurGenerees;
nbValeursActuellesGeneres = 0;

%Param�tres de la fonction (d�pendant des fonctions de densit�s, etc, voir
%PDF)
c = 2/power(log(2),2);

tic 

% Tant qu'on a pas le nombre de valeur voulu
while(nbValeursActuellesGeneres<nbvaleurGenerees)
    %On g�n�re une abscisse et une ordonn�e
    U = rand(1,1);
    V = rand(1,1);
    
    %Le point de l'espace 2D est il sous la courbe de fx ? 
    % le "1" correspond � g(x) (puisqu'on prend g suit une loi uniforme,
    % g(x) est toujours �gal � 1. 
    % Il n'y a pas la fonction indicatrice, car les valeur tir�es par la la
    % loi uniforme sont d�j� sur cet intervale.
    if(V*c*1 <= c*((log(1+U)/(1+U))))
        %Le point est dedans, on le garde
        nbValeursActuellesGeneres = nbValeursActuellesGeneres+1;
        % On stocke sa valeur dans le tableau r�sultat. (absicsse
        % seulement)
        values(nbValeursActuellesGeneres) = U;
    else
        %Le point est pas dedans, on le jette
    end
end

temps = toc;

end