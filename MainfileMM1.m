%% Param�trage de la classe
lambda = 20;
mu = 20;
D = 1000;
temps = 0.5;
alpha = lambda/mu;

nbDeTests = 1;

valuesTempsMoyAttente= linspace(0,0,nbDeTests);
valuesNbMoyenClient= linspace(0,0,nbDeTests);

%Moyennage r�alis� pour comparer avec valeurs th�oriques
for i = 1:nbDeTests
    % Pour le graphique final
[tabArrivees, tabDeparts ] = FileMM1(lambda, mu, D);
tableauPresence = NbClientPresentsMM1(tabArrivees, tabDeparts,D);

tempsMoyAttenteCourant = MoyenneTempsdAttente(tabArrivees, tabDeparts);
%[ sum ] = evolutionFileMM1(tabArrivees, tabDeparts, temps);

%stockage
valuesTempsMoyAttente(i)= tempsMoyAttenteCourant;
valuesNbMoyenClient(i)= mean(tableauPresence);
end

EdeN = (alpha/(1-alpha));
EdeW = EdeN/lambda;

%repr�sentation du temps moyen pratique pass� dans le systeme
display(['Temps moyen pass� dans la file en minutes = ',num2str(60*mean(valuesTempsMoyAttente))]);
%donne des r�sultats valides seulement si lambda > mu
display(['E(W) = Temps moyen th�orique d attente en minutes = ',num2str(60*EdeW)]);

%repr�sentation du nombre de clients pratique dans le systeme
display(['Nombre moyen de clients dans le systeme =  ', num2str(mean(valuesNbMoyenClient))]);
%repr�sentation du nombre de clients th�orique pass� dans le systeme
display(['E(N) = Nombre de clients th�oriques dans le systeme en moyenne = ',num2str(EdeN)]);


%% Histogramme
%% Affichage des transform�es inverses
figure
title('Nb de clients pr�sents')
nbLigne2 = 1;
nbColon2 = 1;
i = 1;
subplot(nbLigne2,nbColon2,i); hold on; stairs(tableauPresence); title('Presence clients'); xlabel('Nb de minutes depuis d�part'); ylabel('Nb de clients'); hold off;
%i = i+1;
%subplot(nbLigne2,nbColon2,i); hold on; histogram(valuesRejet,nbColHistogramme,'Normalization','pdf'); title('Rejet'); plot(x,yFonction,'r'); legend('simulated','theory'); hold off;