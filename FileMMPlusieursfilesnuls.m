function [ tabArrivees, tabDeparts ] = FileMM2(lambda, mu, D, nbDeCaisse)

% lambda = parametre de la loi exponentielle des arrivees
% mu = parametre de la loin exponentielle des departs
% D = temps d'observation de la chaine

% arrivee = vecteur des dates d'arrivee des clients dans la file
% depart = vecteur des dates de d�part des client dans la file
i = 1;

%tabArrivees = linspace(0,0,2000);

tabArrivees(1,1)= Exponentielle(lambda, 1);
lastArriveClient = tabArrivees(1,1);
CaisseFermee = 0;

%%remplissage des arriv�es
while tabArrivees(i,mod(i,nbDeCaisse))<D
    for j = 1:nbDeCaisse
        %On calcule l'heure d'arriv�e du client
        a = lastArriveClient + Exponentielle(lambda, 1);
        % si hors horaire, on jete le client
        if a > D
            CaisseFermee = 1;
            break
        else
            %Sinon, on le met dans la file.
            tabArrivees(i+1,j) = a;
            lastArriveClient = tabArrivees(i+1,j);
            i = i+1;
        end
    end
    
    if CaisseFermee == 1
        break
    end
end

tabDeparts = zeros(length(tabArrivees));


for j = 1:nbDeCaisse
    %%si le premier
    tabDeparts(1,j) = tabArrivees(1,j) + Exponentielle(mu, 1);
    
    %%remplissage des d�parts
    for i = 1:length(tabArrivees)-1
        %Le client apr�s la fin du premier. On le sert de suite.
        if tabArrivees(i+1,j) > tabDeparts(i,j)
            tabDeparts(i+1,j) =  tabArrivees(i+1,j) + Exponentielle(mu, 1);
        else
            % Le client est servi apr�s le d�part du premier
            tabDeparts(i+1,j) =  tabDeparts(i,j) + Exponentielle(mu, 1);
        end
    end
end



