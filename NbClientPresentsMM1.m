function [ y ] = NbClientPresentsMM1(tabArrivees, tabDeparts, D)

x = linspace(0,D,D*60);

%%remplissage du nombre de client
for i = 1:length(x)
        y(i) = evolutionFileMM1(tabArrivees,tabDeparts,x(i));
end

end