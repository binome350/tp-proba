function [ tabArrivees, tabDeparts ] = FileMM2(lambda, mu, D)

% lambda = parametre de la loi exponentielle des arrivees
% mu = parametre de la loin exponentielle des departs
% D = temps d'observation de la chaine

% arrivee = vecteur des dates d'arrivee des clients dans la file
% depart = vecteur des dates de d�part des client dans la file
i = 1;
%tabArrivees = linspace(0,0,2000);

tabArrivees(1)=Exponentielle(lambda, 1);
%Juste pour rentre rdans la boucle (r��crit)

%%remplissage des arriv�es
while tabArrivees(i)<D
    %On calcule l'heure d'arriv�e du client
    a = tabArrivees(i) + Exponentielle(lambda, 1);
    % si hors horaire, on jete le client
    if a > D
        break
    else
        %Sinon, on le met dans la file.
        tabArrivees(i+1) = a;
        i = i+1;
    end
end

tabDeparts = linspace(0,0,length(tabArrivees));

%%si le premier
tabDeparts(1) = tabArrivees(1) + Exponentielle(mu, 1);
    
%%remplissage des d�parts
for i = 1:length(tabArrivees)-1
    %Le client apr�s la fin du premier. On le sert de suite.
    if tabArrivees(i+1) > tabDeparts(i)
        tabDeparts(i+1) =  tabArrivees(i+1) + Exponentielle(mu, 1);
    else
        % Le client est servi apr�s le d�part du premier
        tabDeparts(i+1) =  tabDeparts(i) + Exponentielle(mu, 1);
    end
end


