%% Variables déclarées pour les fonctions inférieures
global sVN;
global sRANDU;
global sMT;
global sSM;

%% Paramétrage de la classe
sVN = 215;
sRANDU = 215;
sSM = 215;
sMT = 215;

nbRealisations = 1000;
nbvaleurGenerees = 1000;
Pvaleur0 = 1:nbRealisations;
Pvaleur1 = 1:nbRealisations;
Pvaleur2 = 1:nbRealisations;
Pvaleur3 = 1:nbRealisations;

%% Génération ensembles de nombres aléatoires et 
for iterations = 1:nbRealisations
    values = generateur( 0, nbvaleurGenerees );
    Pvaleur0(iterations) = Runs(values,1);
    values = generateur( 1, nbvaleurGenerees );
    Pvaleur1(iterations) = Runs(values,1);
    values = generateur( 2, nbvaleurGenerees );
    Pvaleur2(iterations) = Runs(values,1);
    values = generateur( 3, nbvaleurGenerees );
    Pvaleur3(iterations) = Runs(values,1);
end
display('VonNeuman')
mean(Pvaleur0)
display('Randu')
mean(Pvaleur1)
display('Mersenne')
mean(Pvaleur2)
display('Standard Minimal')
mean(Pvaleur3)
