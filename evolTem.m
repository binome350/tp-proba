function [ B , t ] = evolTem( dt )

    A = zeros(600);
    B = 1:600;   
    
    %B = matrice représentant les points de température de 100C
    for i=1:600 
       B(i)=0;
       if i == 488 || i == 489 || i == 503 || i == 504
          B(i) = 100;
       end
    end
    
    %A = matrice de 600x600 représentant la plaque (par ses points
    %d'intersections)
        for i=1:600
                if mod(i,15)==0 && i >= 479 && i<600
                        A(i,i+15) = 1;
                        A(i,i-15) = 1;
                        A(i,i-464) = 1;
                        A(i,i-1) = 1;
                        A(i,i) = -4;
                elseif i>16 && mod(i,15)==1 && i <= 121
                         A(i,i+15) = 1;
                         A(i,i-15)= 1;
                         A(i,i+464) = 1;
                         A(i,i+1) = 1;
                         A(i,i) = -4;
                 %coins 
                 elseif i==1 
                     A(i,i+15) =1;
                     A(i,i+1) =1;
                     A(i,i) = -2;
                 elseif i==15
                     A(i,i+15) = 1;
                     A(i,i-1) = 1;
                     A(i,i) = -2;
                 elseif i==600
                     A(i,i-15) =1;
                     A(i,i-1) = 1;
                     A(i,i) =-2;
                 elseif i==586
                     A(i,i-15) = 1;
                     A(i,i+1) =1;
                     A(i,i) = -2;  
                     %bord gauche
                 elseif i < 15 && i>1 
                     A(i,i+15) = 1;
                     A(i,i+1) = 1;
                     A(i,i-1) = 1;
                     A(i,i) = -3;
                      %bord bas
                 elseif(mod(i,15)==0 && i < 479)
                     A(i,i) = -3;
                     A(i,i-15) = 1;
                     A(i,i+15) = 1;
                     A(i,i-1) = 1;   
                      %bord haut
                 elseif(mod(i,15)==1 && i > 121)
                     A(i,i) = -3;
                     A(i,i-15) = 1;
                     A(i,i+15) = 1;      
                     A(i,i+1) = 1;
                      %bord droit
                 elseif i > 586 && i<600
                     A(i,i-15) = 1;
                     A(i,i-1) = 1;
                     A(i,i+1) = 1;
                     A(i,i) = -3;  
                      
                      %fente
                      %bord gauche
                  elseif (i > 365 && i <= 370)
                      A(i,i-15) = 1;
                      A(i,i-1) = 1;
                      A(i,i+1) = 1;
                      A(i,i) = -3; 
                      %bord droit
                  elseif (i >380 && i <= 385)
                      A(i,i+15) = 1;
                      A(i,i-1) = 1;
                      A(i,i+1) = 1;
                      A(i,i) = -3; 
                      
                else     
                         A(i,i) = -4;
                         A(i,i+15)= 1;
                         A(i,i-15)= 1;
                         A(i,i-1) = 1;
                         A(i,i+1) = 1; 
                end
         end
   
    X = B';
    Xold = X-X;
    t = 0;
    
    while max(max(abs(Xold - X))) >= 0.01       
%         X(488) = 100;
%         X(489) = 100;
%         X(503) = 100;
%         X(504) = 100;
%        
       
       set(gca,'XDir','reverse');
       tmp = reshape(X, 15, 40);
       surf(tmp);
%        zlim([0 100]);
       
       Xold = X;
       X = expm(A*dt)*X;      
       pause(0.00001);
       t = t + dt; 
    end
    t
end

        


