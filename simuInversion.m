function [ values, temps ] = simuInversion(nbvaleurGenerees)
% Tableau de sortie
values = 1:nbvaleurGenerees;

tic;
%G�n�re tableau de valeurs al�atoires uniformes
U = rand(1,nbvaleurGenerees);

%On applique F-1(U)
values = exp(sqrt(U)*log(2))-1;
temps = toc;

end