%% Param�trage de la classe
nbRealisations = 1;
nbvaleurGenerees = 1000;
valuesExp = linspace(0,0,nbvaleurGenerees);
lambda = 0.1;

% Pour le graphique final
x = linspace(0,60,2000);
yFonction = pdf('Exponential',x,1/lambda);
%Equivalent � : yFonction = exppdf(x,lambda);

%for i = 1:nbRealisations
    %% G�n�ration avec exponentielle
    valuesExp = Exponentielle(lambda, nbvaleurGenerees);
%end

%Normalisation. NON Plus ! On perd des infos :)
%valuesExp = valuesExp / max(valuesExp);

%Moyenneage manuel, peut �tre fautif. NON ! 
%valuesExp = valuesExp/nbRealisations;

%% Histogramme
%% Affichage des transform�es inverses
figure
title('Histogramme')
nbColHistogramme = 10000;
hold on; histogram(valuesExp,nbColHistogramme,'Normalization','pdf'); title('Exponentielle'); plot(x,yFonction,'r') ; axis([0 80 0 inf]); legend('simulated','theory'); hold off;