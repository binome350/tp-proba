function [ Pvaleur ] = Frequency( x, nb )
% Fonction qui calcul la proportion de z�ros et de un dans une s�quence enti�re 
% x = vecteur des nombres observ�s
% nb = nb de bits � consid�rer pour chacun de ces nombres
% Pvaleur = 0

% bitget(x(i),1:nb) = nombre N� i converti en binaire
% 2*bitget(x(i),1:nb)-1 = nombre converti en +1/-1

Sn=0;

for i = 1:length(x)
    %On r�cup�re tous les uns de la s�quences
   Sn = Sn + sum(2*bitget(x(i),1:nb)-1);
end

%Calcul de la proportion observ�e
Sobs = abs(Sn)/(sqrt(length(x)));

Pvaleur = 2*(1-cdf('Normal',Sobs,0,1));

end