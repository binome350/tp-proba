%% Param�trage de la classe
nbRealisations = 1;%Probl�me avec plus de une r�alisation. Mon moyennage ne doit pas fonctionner.
nbvaleurGenerees = 1000000;
valuesInverse = linspace(0,0,nbvaleurGenerees);
valuesRejet = linspace(0,0,nbvaleurGenerees);
valuesTMP = linspace(0,0,nbvaleurGenerees);
tempsInverse = linspace(0,0,nbvaleurGenerees);
tempsRejet = linspace(0,0,nbvaleurGenerees);

% Pour le graphique final
x = linspace(0,1,200);
c = 2/power(log(2),2);
yFonction = c*(log(1+x)./(1+x));

for i = 1:nbRealisations
    %% G�n�ration avec simulation par inversion
    [valuesTMP, tempsInverse(i)] = simuInversion(nbvaleurGenerees);
    valuesInverse = valuesInverse + valuesTMP;
    
    %% G�n�ration avec simulation par rejet
    [valuesTMP, tempsRejet(i)] = simuRejet(nbvaleurGenerees);
    valuesRejet = valuesRejet + valuesTMP;
end

%Moyenneage manuel, peut �tre fautif.
%valuesInverse = valuesInverse/nbRealisations;
%valuesRejet = valuesRejet/nbRealisations;

display(['Moyenne temps Invers� : ',num2str(mean(tempsInverse))]);
display(['Moyenne temps Rejet : ',num2str(mean(tempsRejet))]);

%% Histogramme
%% Affichage des transform�es inverses
figure
title('Histogramme')
nbLigne2 = 1;
nbColon2 = 2;
nbColHistogramme = 20;
i = 1;
subplot(nbLigne2,nbColon2,i); hold on; histogram(valuesInverse,nbColHistogramme,'Normalization','pdf'); title('Inversion'); plot(x,yFonction,'r') ; legend('simulated','theory'); hold off;
i = i+1;
subplot(nbLigne2,nbColon2,i); hold on; histogram(valuesRejet,nbColHistogramme,'Normalization','pdf'); title('Rejet'); plot(x,yFonction,'r'); legend('simulated','theory'); hold off;