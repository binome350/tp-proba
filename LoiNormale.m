function [ values ] = LoiNormale( n, p, nbvaleurGenerees)
% Tableau de sortie
values = 1:nbvaleurGenerees;

m = n*p;
phiCarre = n*p*(1-p);

%On g�n�re nos variables al�atoires uniformes
U = rand(1,n);
V = rand(1,n);

%On cr�� un tableau de valeurs (mult terme � terme)
X = sqrt(-log(U)).*cos(2*pi*V);
% Utile ? % Y = sqrt(-log(U)).*sin(2*pi*V);

% translation de la loi N(0,1) � N(m,phiCarre)
Xtranslate = m+phiCarre.*X;

%On stocke
values = Xtranslate;

end