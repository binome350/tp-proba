%% Variables déclarées pour les fonctions inférieures
global sVN;
global sRANDU;
global sMT;
global sSM;

%% Paramétrage de la classe
nbvaleurGenerees = 100000;
sVN = 215;
sRANDU = 215;
sSM = 215;
sMT = 215;

%% Génération Von Neuman
values0 = generateur( 0, nbvaleurGenerees );

%% Génération RANDU
values1 = generateur( 1, nbvaleurGenerees );

%% Génération MERSENNE
values2 = generateur( 2, nbvaleurGenerees );

%% Génération Standard Minimal
values3 = generateur( 3, nbvaleurGenerees);

%% Histogramme
%% Affichage des transformées inverses
figure
title('Histogramme')
nbLigne2 = 2;
nbColon2 = 2;
nbColHistogramme = 20;
i = 1;
max = power(2,31)-1;
subplot(nbLigne2,nbColon2,i);  histogram(values0,nbColHistogramme); title('Von Neuman')
i = i+1;
subplot(nbLigne2,nbColon2,i); histogram(values1,nbColHistogramme);axis([0 max 0 inf]); title('RANDU')
i = i+1;
subplot(nbLigne2,nbColon2,i); histogram(values2,nbColHistogramme);axis([0 max 0 inf]); title('MERSENNE')
i = i+1;
subplot(nbLigne2,nbColon2,i); histogram(values3,nbColHistogramme);axis([0 max 0 inf]); title('Standard Minimal')
