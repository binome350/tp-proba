function [ values ] = Exponentielle(lambda, nbvaleurGenerees)
% Tableau de sortie
values = linspace(0,0,nbvaleurGenerees);

%G�n�re tableau de valeurs al�atoires uniformes
U = rand(1,nbvaleurGenerees);

%On applique F-1(U)
values = -log(1-U)/lambda;

end