function [ Pvaleur ] = Runs( vecteurValAleatoire, nbBitParMot )
% Calcul de la longueur moyenne des "runs" de 000.. ou de 111..

% x vecteur de nombres al�atoires observ�s
% nb nombre de bits � consid�rer pour chacun de ces nombres
% proportion = somme des bits / nb

%% Param�tres de la fonction
nbDeBits = length(vecteurValAleatoire)*nbBitParMot;
Somme = 0;
Pvaleur = 0;

%% Calculer le nombre de 1 dans la s�quence
for i = 1:length(vecteurValAleatoire)
    Somme = Somme + sum(bitget(vecteurValAleatoire(i),1:nbBitParMot));
end

%% Calcul de la proportion de 1 dans la s�quence
proportion = Somme/(nbDeBits);
thau = 2/(sqrt(nbDeBits));

% Condition d'arr�t. Si la proportion n'est pas v�rifi� on ne continue pas.
if abs(proportion-0.5)<thau
    nbSequences = 0;
    sommesLongueurs = 0;
    longueurSequenceActuelle = 0;
    dernierBitMotPrecendent = 0;
    %Calcul des longueurs de s�quences
    for i = 1:length(vecteurValAleatoire)
        %R�cup�re le mot entier comme cha�ne de bit
        motCourant = bitget(vecteurValAleatoire(i),1:nbBitParMot);
        
        %Pour le premier bit du mot courant compar� � la fin du mot
        %pr�c�dent
        if i == 1
            %Si on est sur le premier mot, une s�quence commence forc�ment
            longueurSequenceActuelle = longueurSequenceActuelle + 1;
        else
            if dernierBitMotPrecendent ~= motCourant(1)
                %La chaine actuelle est termin�e
                nbSequences = nbSequences+1;
                sommesLongueurs = sommesLongueurs + longueurSequenceActuelle;
                longueurSequenceActuelle = 0;
            else 
                %La s�quence actuelle continue
                longueurSequenceActuelle = longueurSequenceActuelle + 1;
            end
        end
        
        
        %Pour chaque bit du mot. (sauf le dernier)
        for j = 1:nbBitParMot-1
            if motCourant(j)~=motCourant(j+1)
                %La s�quence actuelle est finie
                nbSequences = nbSequences+1;
                sommesLongueurs = sommesLongueurs + longueurSequenceActuelle;
                longueurSequenceActuelle = 0;
            else
                %La s�quence actuelle continue
                longueurSequenceActuelle = longueurSequenceActuelle + 1;
            end
        end
        
        % Solution tr�s jolie adoptable pour �viter le deuxi�me for.
        % if vecteurValAleatoire(i) ~= vecteurValAleatoire(i+1)
        %   Somme2 = Somme2+1;
        % end
        
        %Pour le dernier bit du mot.
        %Si on est vraiment � la fin fin
        if i == length(vecteurValAleatoire)
            %La chaine actuelle est termin�e
            nbSequences = nbSequences+1;
            sommesLongueurs = sommesLongueurs + longueurSequenceActuelle;
            longueurSequenceActuelle = 0;
        else
            %On est juste � la fin d'un mot = on pr�pare le mot suivant
            dernierBitMotPrecendent = motCourant(nbBitParMot);
        end
        
    end
    
    %Derni�re valeur
    VsObserve = nbSequences;
    
    Pvaleur = 2*(1-cdf('Normal', abs(VsObserve-2*nbDeBits*proportion*(1-proportion))/(2*sqrt(nbDeBits)*proportion*(1-proportion)),0,1));
end


